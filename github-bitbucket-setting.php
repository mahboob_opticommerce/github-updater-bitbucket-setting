<?php
/**
 * Plugin Name:       GitHub Updater Bitbucket setting
 * Plugin URI:        https://github.com/afragen/github-updater
 * Description:       A plugin to automatically update  bitbucket setting for GitHub Updater plugin to fetch private repository from bitbucket
 * Version:           1.0
 */
add_filter( 'github_updater_set_options',
	function () {
		return array( 
			'bitbucket_username'        => 'username',
			'bitbucket_password'        => 'password',
		);
	} );